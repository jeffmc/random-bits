import bs4 as bs
import requests
import csv

file_name = 'movie.csv'

base_url = 'https://yst.am'
page_url = '/browse-movies?page='
url = 'https://yst.am/browse-movies'


def get_movie_info(url):
    soup = bs.BeautifulSoup(get_page_html(url), features="html.parser")
    movie_titles = soup.find_all('a', class_="browse-movie-title")
    try:
        with open(file_name, mode='a') as f:
            writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            for i in movie_titles:
                # print('#' * 15)
                # print(i)
                link = base_url + i.get('href')
                title = i.text
                # print(title)
                # print('?' * 15)
                # print(link)
                writer.writerow([title, link])
    except:
        pass


def get_page_count(soup):
    page_count_element = soup.find_all('li', class_='pagination-bordered')
    for i in page_count_element:
        page_count = int(i.text.split(' ')[2])
    return page_count


def get_page_html(url):
    r = requests.get(url)
    if r.status_code == 200:
        return r.content
    else:
        print("Failed code 200")


def main():
    sp = bs.BeautifulSoup(get_page_html(url), features="html.parser")
    count = get_page_count(sp)
    for i in range(count):
        print(i)
        i = i + 1
        read_url = base_url + page_url + str(i)
        get_movie_info(read_url)
    pass


if __name__ == '__main__':
    main()
    pass